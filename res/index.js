"use strict";
define([
		"file!./_output/layouts.json",
		"file!./_output/styles.json",
		"file!./_output/l18n.json",
		"file!./_output/config.json",
	], function(layoutsJSON, stylesJSON, l18nJSON, configJSON){

	return {
		layouts : JSON.parse(layoutsJSON),
		styles : JSON.parse(stylesJSON),
		l18n : JSON.parse(l18nJSON),
		config : JSON.parse(configJSON),
	};

});