var fs = require("fs");
var jsonfile = require("jsonfile");
var uglify = require("uglify-js");
var package = jsonfile.readFileSync("package.json");
var usConfig = jsonfile.readFileSync("userscript.config.json");
var usSources = fs.readFileSync("userscript.src.js", "utf-8");

usSources = uglify.minify(usSources, {
	output: {
        beautify: false,
        preamble : "/*Baloven*/"
    },
    compress : {
        drop_console : true,
        keep_infinity : true,
        passes : 4,
        sequences : true,
        unsafe_math : true,
        dead_code: true,
        conditionals: true,
        booleans: true,
        unused: true,
        if_return: true,
        join_vars: true,
    },
});

var userScriptMeta = asmUserScriptMetaSctring(package, usConfig);

fs.writeFileSync("launcher.user.js", [userScriptMeta, "(function() {", usSources.code, "})();"].join("\n"), "utf-8", function(){
	console.log(arguments);
})

// console.log(usConfig, package, usSources);

function asmUserScriptMetaSctring(package, usConfig){
	var result = "// ==UserScript==\n";

	for (var k in usConfig){
		if (typeof usConfig[k] == "string"){
			result = addToken2USString(result, k, usConfig[k])
		} else if (typeof usConfig[k] == "object" && usConfig[k].length){
			var tokens = usConfig[k];

			for (var a = 0; a < tokens.length; a++){
				result = addToken2USString(result, k, tokens[a])
			}
		}
	}

	if (package.author) result = addToken2USString(result, "author", package.author);
	if (package.name) result = addToken2USString(result, "name", package.name);
	if (package.version) result = addToken2USString(result, "version", package.version);
	if (package.description) result = addToken2USString(result, "description", package.description);

	result += "\n// ==/UserScript==\n"

	return result;
}

function addToken2USString(string, name, value){
	return string + ["//", "@" + name, value].join(" ") + "\n";
}

function superTrim(input){
	if (typeof input == "number"){
		input = input.toString();
	}

	input = input.replace(/\s\s+/g, " ");
	input = input.replace(/(\r\n|\n|\r)/gm,"");
	input = input.trim();
	return input;
}