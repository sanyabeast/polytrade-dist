var colors = require("colors");

const { exec } = require("child_process");

var target = process.argv[2];
var commitMessage = process.argv[3];

if (!commitMessage){
	if (target){
		commitMessage = target;
		target = "all";
	} else {
		log("not enough data");
	}
}


console.log(target, commitMessage, process.argv);

switch(target){
	case "src":
		pushSrc(commitMessage);
	break;
	case "dist":
		pushDist(commitMessage);
	break;
	case "all":
		pushDist(commitMessage, function(commitMessage){
			pushSrc(commitMessage);			
		});
		
	break;
}

function pushDist(commitMessage, onComplete){
	log(commitMessage.magenta);
	exc("npm run increase", { cwd : "./" }, function(output){
		log("dist version increased".cyan);
		log("dist build started".yellow);

		exc("node scripts/assemble.js", { cwd : "./" }, function(output){
			log("dist build finished".cyan);

			exc("git add -A", { cwd : "" }, function(output){
				log("dist changed added".cyan);

				exc("git commit -m \"" + commitMessage +  "\"", function(output){
					log("dist changed commited".cyan);

					exc("git push origin master", function(output){
						log("dist pushed".cyan);
						if (onComplete) onComplete(commitMessage);
					});

				});

			});

		});

		// exec("")

		// console.log(output);
	});
}

function pushSrc(commitMessage, onComplete){
	log(commitMessage.magenta);
	exc("git status", { cwd : "src" }, function(output){
		switch(true){
			case !output:
				log("no `git status` output :(".yellow);
			break;
			case output.indexOf("Changes not staged for commit") > -1:
				exc("git add -A", { cwd : "src" }, function(output){
					log("src changed added".cyan);
					exc("git commit -m \"" + commitMessage + "\"", { cwd : "src" }, function(output){
						log("src changes commited".cyan);
						exc("git push origin master", { cwd : "src" }, function(output){
							log("src changed pushed".cyan);
							if (onComplete) onComplete(commitMessage);
						});
					});
				});	

				
			break
			case output.indexOf("Changes to be committed" > -1):
				log("src changed added".cyan);
				exc("git commit -m \"" + commitMessage + "\"", { cwd : "src" }, function(output){
					log("src changes commited".cyan);
					exc("git push origin master", { cwd : "src" }, function(output){
						log("src changed pushed".cyan);
						if (onComplete) onComplete(commitMessage);
					});
				});	
			break;
			case output.indexOf("Your branch is ahead") > -1:
				log("src changes commited".cyan);
				exc("git push origin master", { cwd : "src" }, function(output){
					log("src changed pushed".cyan);
					if (onComplete) onComplete(commitMessage);
				});
			break;
			case output.indexOf("Your branch is up to date") > -1:
				log("src changed pushed".cyan);
				if (onComplete) onComplete(commitMessage);
			break;
		}

	});
}


function onErr(err){
	log("ERROR! ".red, err);
}

function log(data){
	console.log("push-wizard: ".green, data);
}

function exc(command, options, callback){
	exec(command, options, function(err, output){
		if (err) { 
			onErr(err);
			return;
		}

		callback(output);

	});
}